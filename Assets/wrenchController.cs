﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrenchController : MonoBehaviour {
    public float rotSpeed;
    public float upFac;
    public float downFac;
    public GameObject wrenchHolder;
    public GameObject dummyWrench;
    public GameObject wrenchMover;
    public Transform pivot_one;
    public Transform pivot_two;
    public bool isflipped;
    public Transform pivot;
    public Vector3 pivotPos;
    public Transform otherPivot;
    public bool isCloseToScrew;

    //public Transform[] screws;
    public GameObject[] screws;
    //public GameObject[] screwss;
    public List<GameObject> screwList;
    //public Transform[] screwsTest;
    public Vector3 screwPos;
    public Vector3 hookScrewPos;
    public float dist;
    public float tempDist;
    public float snapRange;
    Vector3 desiredPos;

    public bool isScrewTimerReset;

    public Vector3 jumpedPos;

    public bool isScrewUp;
    public bool isScrewDown;
    public bool isScrewRight;
    public bool isScrewLeft;

    public Vector2 mousePos;
    public Vector2 startMousePos;
    public Vector2 endMousePos;
    public Vector2 swipeTravel;

    public bool swipeUp;
    public bool swipeDown;
    public bool swipeRight;
    public bool swipeLeft;

    public bool isScrewMoved;
    public bool isSwiped;



    // Use this for initialization
    void Start () {
        //screws = GameObject.FindGameObjectsWithTag("screws");
        screws = screwList.ToArray();
        pivot = pivot_one;
        otherPivot = pivot_two;
        isScrewTimerReset = false;
        jumpedPos = this.transform.position;
        dummyWrench = GameObject.FindGameObjectWithTag("wrenchDummy");

    }
	
	// Update is called once per frame
	void Update () {

        //JumpWrench();
        screws = screwList.ToArray();
        checkScrewPos();
        SensingScrews();
        //rotateWrench();
        //swingWrench();
        
        //flipWrench();
        
        //checkScrewPos();
        pivotPos = pivot.transform.position;
        Swiping();
        //JumpWrench();

        //this.transform.position = Vector3.Lerp(this.transform.position, desiredPos, Time.deltaTime * 30f);

    }

    void rotateWrench()
    {
        //wrenchMover.transform.Rotate(0, 0, rotSpeed+upFac+downFac );
        wrenchMover.transform.Rotate(0, 0, rotSpeed );
        dummyWrench.transform.Rotate(0, 0, rotSpeed);
    }
    //void swingWrench()
    //{
    //    Vector3 rot = Quaternion.ToEulerAngles(wrenchMover.transform.localRotation);
    //    //upFac = Mathf.Lerp(5, 0, (rot.z*57.3f) / 180);
    //    //downFac = Mathf.Lerp(5,0, (rot.z * 57.3f) / -180);
    //    if(rot.z > 0)
    //    {
    //        downFac = Mathf.Lerp(downFac, 0, Time.deltaTime* 20f);
    //    }
    //    else
    //    {
    //        downFac = Mathf.Lerp(5, 0, (rot.z * 57.3f) / -180);
    //    }
    //    if (rot.z < 0)
    //    {
    //        upFac = Mathf.Lerp(upFac, 0, Time.deltaTime * 20f);
    //    }
    //    else
    //    {
    //        upFac = Mathf.Lerp(5, 0, (rot.z * 57.3f) / 180);
    //    }
    //
    //
    //
    //}

    void flipWrench()
    {
        //if(isflipped == false)
        //{
        //    pivot = pivot_one;
        //    otherPivot = pivot_two;
        //    //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
        //}
        //else
        //{
        //    pivot = pivot_two;
        //    otherPivot = pivot_one;
        //    //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
        //}
        //dist = Vector3.Distance(hookScrewPos, otherPivot.transform.position);

        if (Input.GetMouseButtonDown(0))
        {
            checkScrewPos();
            if (isflipped == false)
            {
                //dist = Vector3.Distance(screwPos, pivot_two.transform.position);
                if(dist< snapRange)
                {
                    
                    this.transform.position = hookScrewPos;
                    wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
                    isScrewTimerReset = true;
                    Invoke("ResetScrewTimer", 0.5f);
                    //desiredPos = hookScrewPos;
                    //this.transform.position = Vector3.Lerp(this.transform.position, hookScrewPos, Time.deltaTime * 10f);
                }
                //else
                //{
                //    this.transform.position = pivot_two.transform.position;
                //    //desiredPos = pivot_two.transform.position;
                //    this.GetComponent<Rigidbody>().useGravity = true;
                //}
                //this.transform.position = pivot_two.transform.position;
                //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
            }
            else
            {
                //dist = Vector3.Distance(screwPos, pivot_one.transform.position);
                if (dist < snapRange)
                {
                    //this.transform.position = screwPos;
                    this.transform.position = hookScrewPos;
                    wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
                    isScrewTimerReset = true;
                    Invoke("ResetScrewTimer", 0.5f);
                    //desiredPos = hookScrewPos;
                    //this.transform.position = Vector3.Lerp(this.transform.position, hookScrewPos, Time.deltaTime * 10f);
                }
                //else
                //{
                //    this.transform.position = pivot_one.transform.position;
                //    //desiredPos = pivot_one.transform.position;
                //    this.GetComponent<Rigidbody>().useGravity = true;
                //    //this.GetComponent<Rigidbody>().AddForce(this.transform.position.x * 300, 0, -500);
                //}
                //this.transform.position = pivot_one.transform.position;
                //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
            }

            if (isflipped == false)
            {
                isflipped = true;
            }
            else
            {
                isflipped = false;
            }
        }
        if (isflipped == false)
        {
            pivot = pivot_one;
            otherPivot = pivot_two;
            //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
        }
        else
        {
            pivot = pivot_two;
            otherPivot = pivot_one;
            //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
        }
        dist = Vector3.Distance(hookScrewPos, otherPivot.transform.position);
    }

    void JumpWrench()
    {

        dummyWrench.transform.position = Vector3.Lerp(dummyWrench.transform.position, jumpedPos, Time.deltaTime *20f);
        //wrenchMover.transform.position = Vector3.Lerp(wrenchMover.transform.position, jumpedPos, Time.deltaTime * 10f);
        this.transform.position = jumpedPos;
        if (isScrewMoved == false)
        {
            if (swipeUp == true && isScrewUp == true)
            {
                //this.transform.position += new Vector3(0, 4, 0);
                jumpedPos += new Vector3(0, 4, 0);
                ResetSwipes();
                ResetSensingScrews();
            }
            else if (swipeDown == true && isScrewDown == true)
            {
                //this.transform.position += new Vector3(0, -4, 0);
                jumpedPos += new Vector3(0, -4, 0);
                ResetSwipes();
                ResetSensingScrews();
            }
            else if (swipeRight == true && isScrewRight == true)
            {
                //this.transform.position += new Vector3(4, 0, 0);
                jumpedPos += new Vector3(4, 0, 0);
                ResetSwipes();
                ResetSensingScrews();
            }
            else if (swipeLeft == true && isScrewLeft == true)
            {
                //this.transform.position += new Vector3(-4, 0, 0);
                jumpedPos += new Vector3(-4, 0, 0);
                ResetSwipes();
                ResetSensingScrews();
            }
            //isScrewMoved = true;
        
        }
        

    }
    void Swiping()
    {
        mousePos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            ResetSensingScrews();
            startMousePos = endMousePos = mousePos;
            isSwiped = false;
        }
        if (Input.GetMouseButton(0))
        {
            endMousePos = mousePos;
        }
        if (Input.GetMouseButtonUp(0))
        {
            startMousePos = endMousePos = mousePos;
            ResetSwipes();
            //ResetSensingScrews();

            //Invoke("JumpWrench", 0.01f);
        }

        swipeTravel = (10 * (startMousePos - endMousePos)) / Screen.width;

        if ((Mathf.Abs((swipeTravel.x)) > 1 || Mathf.Abs((swipeTravel.y)) > 1) && isSwiped == false)
        {
            if (Mathf.Abs((swipeTravel.x)) > Mathf.Abs((swipeTravel.y)))
            {
                if ((swipeTravel.x) < 0)
                {
                    swipeRight = true;
                }
                else if ((swipeTravel.x) > 0)
                {
                    swipeLeft = true;
                }

            }
            else if (Mathf.Abs((swipeTravel.x)) < Mathf.Abs((swipeTravel.y)))
            {
                if ((swipeTravel.y) < 0)
                {
                    swipeUp = true;
                }
                else if ((swipeTravel.y) > 0)
                {
                    swipeDown = true;
                }
            }
        }
        JumpWrench();


    }
    void ResetSwipes()
    {
        swipeUp = false;
        swipeDown = false;
        swipeRight = false;
        swipeLeft = false;
        isSwiped = true;
    }
    void checkScrewPos()
    {

        
        

        for (int i = 0; i < screws.Length; i++)
        {
            screwPos = screws[i].transform.position;
            tempDist = Vector3.Distance(screwPos, otherPivot.transform.position);
            if(tempDist < snapRange)
            {
                dist = tempDist;
                hookScrewPos = screwPos;
            }
            //if (isflipped == false)
            //{
            //    dist = Vector3.Distance(screwPos, pivot_two.transform.position);
            //    if(dist< 0.5f)
            //    {
            //        isCloseToScrew = true;
            //    }
            //    else
            //    {
            //        isCloseToScrew = false ;
            //    }
            //    //this.transform.position = pivot_two.transform.position;
            //    //wrenchHolder.transform.localPosition = new Vector3(0, 2, 0);
            //}
            //else
            //{
            //    dist = Vector3.Distance(screwPos, pivot_one.transform.position);
            //    if (dist < 0.5f)
            //    {
            //        isCloseToScrew = true;
            //    }
            //    else
            //    {
            //        isCloseToScrew = false;
            //    }
            //    //this.transform.position = pivot_one.transform.position;
            //    //wrenchHolder.transform.localPosition = new Vector3(0, -2, 0);
            //}
        }
    }

    void ResetScrewTimer()
    {
        if(isScrewTimerReset == true)
        {
            isScrewTimerReset = false;
        }
    }
    void SensingScrews()
    {
        for (int i = 0; i < screws.Length; i++)
        {
            if(this.transform.position + new Vector3(0,4,0) == screws[i].transform.position)
            {
                isScrewUp =true;
            }
            else if (this.transform.position - new Vector3(0, 4, 0) == screws[i].transform.position)
            {
                isScrewDown = true;
            }
            else if (this.transform.position + new Vector3(4, 0, 0) == screws[i].transform.position)
            {
                isScrewRight = true;
            }
            else if (this.transform.position - new Vector3(4, 0, 0) == screws[i].transform.position)
            {
                isScrewLeft = true;
            }
            
        }
    }
    void AssignScrews()
    {
        
    }
    
    
    void ResetSensingScrews()
    {
        isScrewUp = false;
        isScrewDown = false;
        isScrewRight = false;
        isScrewLeft = false;
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag== "screws")
        {
            
            screwList.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "screws")
        {
            
            screwList.Remove(other.gameObject);
        }
    }
}
