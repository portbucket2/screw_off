﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformManager : MonoBehaviour {
    public GameObject screwLeft;
    public GameObject screwRight;
    public GameObject screwLeftMesh;
    public GameObject screwRightMesh;
    public GameObject leftHolder;
    public GameObject rightHolder;
    public GameObject platformBase;
    public GameObject leftRamp;
    public GameObject rightRamp;

    public Color platformColor;

    GameObject wrench;

    public bool leftScrewOff;
    public bool rightScrewOff;

    public bool opening;
    public bool open;

    public bool leftScrewDeActive;
    public bool rightScrewDeActive;

    public bool leftSlopeRamp;
    public bool RightSlopeRamp;



    public Material screwMatRed;
    public Material screwMatGreen;
    public Material screwMatGrey;

    public float screwingTimer;
    public float screwOpeningTime;

    // Use this for initialization
    void Start () {
        
        wrench = GameObject.FindGameObjectWithTag("wrench");
        //screwLeft.GetComponent<Renderer>().material = screwMatRed;
        //screwRight.GetComponent<Renderer>().material = screwMatRed;
        screwLeftMesh.GetComponent<Renderer>().material.color = platformColor;
        screwRightMesh.GetComponent<Renderer>().material.color = platformColor;
        //platformBase.GetComponent<Renderer>().material.color = platformColor;
        if(leftScrewDeActive && rightScrewDeActive)
        {
            platformBase.GetComponent<Renderer>().material = screwMatGrey;
        }
        open = false;
        Ramping();
    }
	
	// Update is called once per frame
	void Update () {
        FallPlatform();
        if(wrench.GetComponent<wrenchController>().isScrewTimerReset == true)
        {
            ResetScrewTimer();
        }
        
        if(open == false)
        {
            OpenScrew();
        }
        DeactivateScrews();
        if (open == true)
        {
            Invoke("GreyIt", 1f);

        }
        //OpenScrew();

    }

    void FallPlatform()
    {
        if(leftScrewOff)
        {
            rightHolder.transform.rotation = Quaternion.Lerp(rightHolder.transform.rotation, Quaternion.Euler(new Vector3(0, 0, 90)), Time.deltaTime * 2f);
        }
        else if (rightScrewOff)
        {
            leftHolder.transform.rotation = Quaternion.Lerp(leftHolder.transform.rotation, Quaternion.Euler(new Vector3(0, 0, -90)), Time.deltaTime * 2f);
        }
    }

    void OpenScrew()
    {
        if(Vector3.Distance(screwLeft.transform.position, wrench.GetComponent<wrenchController>().pivotPos) < 0.1f)
        {
            opening = true;
            screwingTimer += Time.deltaTime;
            if(screwingTimer > screwOpeningTime && leftScrewDeActive == false)
            {
                leftScrewOff = true;
                open = true;
                screwLeftMesh.GetComponent<Renderer>().material = screwMatGreen;
            }
        }
        else if (Vector3.Distance(screwRight.transform.position, wrench.GetComponent<wrenchController>().pivotPos) < 0.1f)
        {
            opening = true;
            screwingTimer += Time.deltaTime;
            if (screwingTimer > screwOpeningTime && rightScrewDeActive == false)
            {
                rightScrewOff = true;
                open = true;
                screwRightMesh.GetComponent<Renderer>().material = screwMatGreen;
            }
        }
        else
        {
            opening = false;
            ResetScrewTimer();
        }
    }

    void GreyIt()
    {
        //screwLeftMesh.GetComponent<Renderer>().material = screwMatGrey;
        //screwRightMesh.GetComponent<Renderer>().material = screwMatGrey;
        screwLeftMesh.SetActive(false);
        screwRightMesh.SetActive(false);
    }

    void DeactivateScrews()
    {
        if(leftScrewDeActive == true)
        {
            //screwLeftMesh.GetComponent<Renderer>().material = screwMatGrey;
            screwLeftMesh.SetActive(false);
        }
        if (rightScrewDeActive == true)
        {
            //screwRightMesh.GetComponent<Renderer>().material = screwMatGrey;
            screwRightMesh.SetActive(false);
        }
    }
    void ResetScrewTimer()
    {
        screwingTimer = 0;
    }

    void Ramping()
    {
        if(leftSlopeRamp == true)
        {
            leftRamp.transform.localRotation = Quaternion.Euler(0, 0, 10);
        }
        else if (RightSlopeRamp == true)
        {
            rightRamp.transform.localRotation = Quaternion.Euler(0, 0, -10);
        }
    }

}
