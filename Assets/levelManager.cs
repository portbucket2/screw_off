﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class levelManager : MonoBehaviour {

    public int level;
    public Text levelValue;
    public int ballsCollected;
    public GameObject bucket;
    public bool gotAllBalls;


	// Use this for initialization
	void Start () {
        ballsCollected = 0;
        if (level == 0)
        {
            level = 1;
        }
        gotAllBalls = false;

        level = PlayerPrefs.GetInt("level", level);
        levelValue.text = "" + level;
        bucket = GameObject.FindGameObjectWithTag("bucket");
    }
	
	// Update is called once per frame
	void Update () {

        ballsCollected = bucket.GetComponent<collectedBalls>().ballsCollected;
        if (bucket.GetComponent<collectedBalls>().ballsCollected == 3 && gotAllBalls == false)
        {
            Invoke("nextLevel", 1f);
            gotAllBalls = true;
        }
		
	}

    public void StartLevel()
    {
        SceneManager.LoadScene(level);
    }
    public void nextLevel()
    {
        level += 1;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        SceneManager.LoadScene(level);
    }
    public void GoToMenu()
    {
        
        SceneManager.LoadScene(0);
    }
}
