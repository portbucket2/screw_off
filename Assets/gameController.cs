﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour {

    public int level;
    public GameObject panelLevels;
    public Text levelValue;

	// Use this for initialization
	void Start () {
        if(level == 0)
        {
            level = 1;
        }

        level = PlayerPrefs.GetInt("level", level);
        levelValue.text = "" + level;
        panelLevels.SetActive(false);


    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            //UpdateLevel();
        }
        if (Input.GetMouseButtonDown(1))
        {
            //ResetLevels();
        }
    }

    public void UpdateLevel()
    {
        level += 1;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
    }

    public void StartLevel()
    {
        SceneManager.LoadScene(level);
    }

    public void ResetLevels()
    {
        level = 1;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
    }

    public void GetToPanelLevels()
    {
        panelLevels.SetActive(true);
    }
    public void backToMenu()
    {
        panelLevels.SetActive(false);
    }

    public void GoToLevel1()
    {
        level = 1;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel2()
    {
        level = 2;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel3()
    {
        level = 3;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel4()
    {
        level = 4;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel5()
    {
        level = 5;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel6()
    {
        level = 6;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel7()
    {
        level = 7;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel8()
    {
        level = 8;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel9()
    {
        level = 9;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel10()
    {
        level = 10;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel11()
    {
        level = 11;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel12()
    {
        level = 12;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel13()
    {
        level = 13;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel14()
    {
        level = 14;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }
    public void GoToLevel15()
    {
        level = 15;
        PlayerPrefs.SetInt("level", level);
        levelValue.text = "" + level;
        StartLevel();
    }


}
